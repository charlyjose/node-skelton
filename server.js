var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require('morgan');
var randomstring = require('randomstring');
var randomInt = require('random-int');

const mysql = require('mysql');


// Database connection configurations
const mc = mysql.createConnection({
	host: 'XXXXXX',
	user: 'XXXXXX',
	password: 'XXXXXX',
	database: 'XXXXXX',
	port: XXXXXX
});

mc.connect(function (error) {
	if (!error) {
		console.log("Database is connected...\n");
	}
	else {
		console.log("Error connecting database...\n");
	}
});


// Invoke an instance of express application.
var app = express();

// Set our application port
app.set('port', 8080);

// Set morgan to log info about our requests for development use.
app.use(morgan('dev'));

// Initialize body-parser to parse incoming parameters requests to req.body
app.use(bodyParser.urlencoded({ extended: true }));

// Initialize cookie-parser to allow us access the cookies stored in the browser.
app.use(cookieParser());

// Initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
    key: 'user_sid',
    secret: 'XXXXXXXXXXXXXXXXXX',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 1200000 // In 20 minutes session Id expires   //// In 25 minutes 1500000 ms Id expires  //// In 15 minutes 900000 ms Id expires
    }
}));

// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
app.use((req, res, next) => {
	if (req.cookies.user_sid && !req.session.user) {
		res.clearCookie('user_sid');
	}
	next();
});

// Route for user coordinator
app.get('/', (req, res) => {
    res.redirect('/home');
});

// Route for user registration
app.get('/home', (req, res) => {
    console.log("\nRequest: " + req.url);
    res.sendFile(__dirname + '/public/home/home.html');
});



// Your Stuff here











//End of stuff


// Route for handling 404 requests
app.use(function (req, res, next) {
   res.status(404).sendFile(__dirname + '/public/error/404error.html');
});

// Start the express server
app.listen(app.get('port'), () => console.log(`App started on port ${app.get('port')}`));





















